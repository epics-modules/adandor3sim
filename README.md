# ADAndor3sim

European Spallation Source ERIC Site-specific EPICS module: ADAndor3sim

Additonal information:
* [Documentation](https://confluence.esss.lu.se/display/IS/How+to+setup+a+SIMCAM+via+Andor+SDK3+and+control+it+with+an+IOC+using+ADAndor3+EPICS+module)
* [Release notes](RELEASE.md)
* [Requirements](https://gitlab.esss.lu.se/nice/staging-recipes/ADAndor3sim-recipe/-/blob/master/recipe/meta.yaml#L18)
* [Building and Testing](https://confluence.esss.lu.se/display/IS/1.+Development+locally+with+e3+and+Conda#id-1.Developmentlocallywithe3andConda-BuildanEPICSmoduleandtestitlocally)
